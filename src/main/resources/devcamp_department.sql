-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2023 at 07:37 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devcamp_department`
--

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` bigint(20) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `function` varchar(255) DEFAULT NULL,
  `introduction` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `code`, `function`, `introduction`, `name`) VALUES
(1, 'FIN', 'Kế toán', 'Kế toán, thanh toán, thuế, lập và trình bày BCTC', 'Tài chính'),
(2, 'HR', 'Tuyển dụng, nhân sự', 'Phụ trách tuyển dụng, đào tạo, đánh giá', 'Nhân sự'),
(3, 'IT', 'Quản trị mạng, thiết bị CNTT, quản lý CSDL, viết phần mềm sử dụng nội bộ', 'Bộ phận phụ trách CNTT', 'Công nghệ Thông tin');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `department_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `address`, `code`, `date_of_birth`, `gender`, `level`, `name`, `phone`, `department_id`) VALUES
(1, 'Hà Nội', 'LTL', '1998-01-01', 'Nữ', 'Kế toán', 'Lại Thùy Linh', '0912345678', 1),
(3, 'Hà Nội', 'NTL', '1998-01-01', 'Nữ', 'Trưởng phòng', 'Nguyễn Thùy Linh', '1234567890', 2),
(4, 'Hà Nội', 'NDM', '2000-01-01', 'Nam', 'Trưởng phòng', 'Nguyễn Đức Minh', '091111111', 3),
(5, 'TP Hồ Chí Minh', 'NVA', '2002-01-01', 'Nam', 'Nhân viên', 'Nguyễn Văn Anh', '098888888', 3),
(6, 'TP Hồ Chí Minh', 'VDA', '1993-12-31', 'Nam', 'Nhân viên', 'Vũ Đức Anh', '090000001', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_nbyivu8qgmx0r7wtbplf01gf8` (`code`),
  ADD UNIQUE KEY `UK_buf2qp04xpwfp5qq355706h4a` (`phone`),
  ADD KEY `FKbejtwvg9bxus2mffsm3swj3u9` (`department_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `FKbejtwvg9bxus2mffsm3swj3u9` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
