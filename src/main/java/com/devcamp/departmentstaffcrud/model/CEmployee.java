package com.devcamp.departmentstaffcrud.model;

import java.util.Date;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table (name = "employee")
public class CEmployee {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long id;

    @Column (name = "code", unique = true)
    private String code;

    @Column (name = "name")
    private String name;

    @Column (name = "level")
    private String level;

    @Column (name = "gender")
    private String gender;

    @Temporal(TemporalType.DATE)
    @JsonFormat (pattern = "yyyy-MM-dd")
    @Column (name = "date_of_birth")
    private Date dateOfBirth;

    @Column (name = "address")
    private String address;

    @Column (name = "phone", unique = true)
    private String phone;

    @ManyToOne
    @JsonIgnore
    @JoinColumn (name = "department_id")
    private CDepartment department;
    
    public CEmployee() {
    }

    public CEmployee(long id, String code, String name, String level, String gender, Date dateOfBirth, String address,
            String phone, CDepartment department) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.level = level;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.phone = phone;
        this.department = department;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public CDepartment getDepartment() {
        return department;
    }

    public void setDepartment(CDepartment department) {
        this.department = department;
    }

    
}
