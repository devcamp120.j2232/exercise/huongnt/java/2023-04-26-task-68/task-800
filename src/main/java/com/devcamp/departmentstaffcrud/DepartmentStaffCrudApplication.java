package com.devcamp.departmentstaffcrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DepartmentStaffCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(DepartmentStaffCrudApplication.class, args);
	}

}
