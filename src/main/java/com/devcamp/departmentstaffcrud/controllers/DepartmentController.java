package com.devcamp.departmentstaffcrud.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.departmentstaffcrud.model.CDepartment;
import com.devcamp.departmentstaffcrud.repository.IDepartmentRepository;

import java.util.*;

@RestController
@CrossOrigin
@RequestMapping ("/department")
public class DepartmentController {
    @Autowired
    IDepartmentRepository departmentRepository;
    @GetMapping("/all")
    public ResponseEntity<List<CDepartment>> getAllDepartment(){
        try {
            List<CDepartment> departmentList = new ArrayList<>();
            departmentRepository.findAll().forEach(departmentList::add);
            return new ResponseEntity<>(departmentList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    
    @GetMapping("detail/{id}")
    public ResponseEntity<CDepartment> getDepartmentById(@PathVariable long id){
        try {
            CDepartment department = departmentRepository.findById(id);
            if (department != null){
                return new ResponseEntity<>(department, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createDepartment(@RequestBody CDepartment pDepartment){
        try {
            CDepartment newDepartment = new CDepartment();
            newDepartment.setCode(pDepartment.getCode());
            newDepartment.setName(pDepartment.getName());
            newDepartment.setFunction(pDepartment.getFunction());
            newDepartment.setIntroduction(pDepartment.getIntroduction());
            newDepartment.setEmployees(pDepartment.getEmployees());
            CDepartment _department = departmentRepository.save(newDepartment);
            return new ResponseEntity<>(_department, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Department: "+e.getCause().getCause().getMessage());

        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateDepartmentById(@PathVariable("id") long id, @RequestBody CDepartment pDepartment){
        try {
            CDepartment department = departmentRepository.findById(id);
            if (department != null){
                department.setCode(pDepartment.getCode());
                department.setName(pDepartment.getName());
                department.setFunction(pDepartment.getFunction());
                department.setIntroduction(pDepartment.getIntroduction());
                
                return new ResponseEntity<>(departmentRepository.save(department), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to update specified Department: "+e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/delete/{id}")
	public ResponseEntity<CDepartment> deleteDepartmentById(@PathVariable("id") long id) {
		try {
			departmentRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
