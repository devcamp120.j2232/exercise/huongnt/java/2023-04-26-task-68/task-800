package com.devcamp.departmentstaffcrud.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.departmentstaffcrud.model.CDepartment;
import com.devcamp.departmentstaffcrud.model.CEmployee;
import com.devcamp.departmentstaffcrud.repository.IDepartmentRepository;
import com.devcamp.departmentstaffcrud.repository.IEmployeeRepository;

import java.util.*;

@RestController
@CrossOrigin
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    IEmployeeRepository employeeRepository;
    @GetMapping("/all")
    public ResponseEntity<List<CEmployee>> getAllEmployee(){
        try {
            List<CEmployee> employeeList = new ArrayList<>();
            employeeRepository.findAll().forEach(employeeList::add);
            return new ResponseEntity<>(employeeList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    
    @GetMapping("detail/{id}")
    public ResponseEntity<CEmployee> getEmployeeById(@PathVariable long id){
        try {
            CEmployee employee = employeeRepository.findById(id);
            if (employee != null){
                return new ResponseEntity<>(employee, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Autowired
    IDepartmentRepository departmentRepository;
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createEmployee(@PathVariable("id") long id, @RequestBody CEmployee pEmployee){
        try {
            CDepartment department = departmentRepository.findById(id);
            if (department != null){
                CEmployee newEmployee = new CEmployee();
                newEmployee.setCode(pEmployee.getCode());
                newEmployee.setName(pEmployee.getName());
                newEmployee.setLevel(pEmployee.getLevel());
                newEmployee.setGender(pEmployee.getGender());
                newEmployee.setDateOfBirth(pEmployee.getDateOfBirth());
                newEmployee.setAddress(pEmployee.getAddress());
                newEmployee.setPhone(pEmployee.getPhone());
                newEmployee.setDepartment(department);
                CEmployee _employee = employeeRepository.save(newEmployee);
                return new ResponseEntity<>(_employee, HttpStatus.CREATED);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Employee: "+e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateEmployeeById(@PathVariable("id") long id,  @RequestBody CEmployee pEmployee){
        try {
            CEmployee employee = employeeRepository.findById(id);
            if (employee != null){
                employee.setCode(pEmployee.getCode());
                employee.setName(pEmployee.getName());
                employee.setLevel(pEmployee.getLevel());
                employee.setGender(pEmployee.getGender());
                employee.setDateOfBirth(pEmployee.getDateOfBirth());
                employee.setAddress(pEmployee.getAddress());
                employee.setPhone(pEmployee.getPhone());
                
                return new ResponseEntity<>(employeeRepository.save(employee), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Update specified Employee: "+e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/delete/{id}")
	public ResponseEntity<CEmployee> deleteEmployeeById(@PathVariable("id") long id) {
		try {
			employeeRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


}
